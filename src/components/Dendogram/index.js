import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';
import Typography from '@mui/material/Typography';

const TREE = d3.tree;
const R = 6;
const PADDING = 1;
const FILL_PRODUCT = '#faff00';
const FILL_CATEGORY = '#0580fc';
const FILL_OPACITY = 0.3;
const STROKE = '#555';
const STROKE_WIDTH = 2.5;
const STROKE_OPACITY = 0.2;
const HALO = '#fff';
const HALO_WIDTH = 3;
const CURVE = d3.curveBumpX;

// Copyright 2021 Observable, Inc.
// Released under the ISC license.
// https://observablehq.com/@d3/tree
const Tree = ({ data, onOpenMoveDialog, width }) => {
  const label = (d) => (d.name);
  const svgRef = useRef(null);
  // If id and parentId options are specified, or the path option, use d3.stratify
  // to convert tabular data to a hierarchy; otherwise we assume that the data is
  // specified as an object {children} with nested objects (a.k.a. the “flare.json”
  // format), and use d3.hierarchy.
  useEffect(() => {
    const root = d3.hierarchy(data);

    // Compute labels and titles.
    const descendants = root.descendants();
    const L = label == null ? null : descendants.map((d)=> (d?.data?.name));

    // Compute the layout.
    const dx = 20;
    const dy = width / (root.height + PADDING);
    TREE().nodeSize([dx, dy])(root);

    // Center the tree.
    let x0 = Infinity;
    let x1 = -x0;
    root.each(d => {
      if (d.x > x1) x1 = d.x;
      if (d.x < x0) x0 = d.x;
    });

    // Compute the default height.
    const height = x1 - x0 + dx * 2;

    // Use the required curve
    if (typeof CURVE !== "function") throw new Error(`Unsupported curve`);

    const svgEl = d3.select(svgRef.current);
    svgEl.selectAll("*").remove();

    const svg = svgEl
      .attr("viewBox", [-dy * PADDING / 2, x0 - dx, width, height])
      .attr("width", width)
      .attr("height", height)
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;")
      .attr("font-family", "sans-serif")
      .attr("font-size", 10);

    svg.append("g")
      .attr("fill", "none")
      .attr("stroke", STROKE)
      .attr("stroke-opacity", STROKE_OPACITY)
      .attr("stroke-width", STROKE_WIDTH)
      .style("cursor", "pointer")
      .selectAll("path")
        .data(root.links())
        .join("path")
          .attr("d", d3.link(CURVE)
              .x(d => d.y)
              .y(d => d.x));

    const node = svg.append("g")
      .selectAll("a")
      .data(root.descendants())
      .join("g")
      .style("cursor", "pointer")

      .attr("transform", d => (`translate(${d.y},${d.x})`));

    node.append("circle")
      .attr("fill", ({ data }) => (
        data.type === 'product' ? FILL_PRODUCT : FILL_CATEGORY
      ))
      .attr("fill-opacity", ({ children, data }) => (
        children || data.type === 'product' ? 1 : FILL_OPACITY
      ))
      .on("click", (d, { data }) => (onOpenMoveDialog(data.id, data)))
      .style("cursor", "pointer")
      .attr("r", R);

    if (L) node.append("text")
      .attr("dy", "0.4em")
      .attr("x", d => d.children ? -10 : 10)
      .attr("text-anchor", d => d.children ? "end" : "start")
      .attr("paint-order", "stroke")
      .attr("stroke", HALO)
      .attr("stroke-width", HALO_WIDTH)
      .on("click", (d, { data }) => (onOpenMoveDialog(data.id, data)))
      .attr("fill-opacity", ({ children, data }) => (
        children || data.type === 'product' ? 1 : FILL_OPACITY
      ))
      .text((d, i) => L[i]);
  });

  return (
    <div>
      <Typography variant="h6" gutterBottom>
        Дендограмма
      </Typography>
      <svg ref={svgRef} />
    </div>
  );
};

export default Tree;
