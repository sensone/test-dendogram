import React, { useState, useCallback, useEffect } from 'react';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const DialogMoveProduct = ({ open, onMove, onClose, categories, item }) => {
  const [selected, setSelected] = useState(item?.parentId || 'none');

  const handleChange = useCallback((event) => (
    setSelected(event?.target?.value)
  ), [setSelected]);

  const onConfirm = useCallback(() => (
    onMove(selected === 'none' ? undefined : selected)
  ), [selected, onMove]);

  useEffect(() => setSelected(item?.parentId), [item]);

  return (
    <Dialog disableEscapeKeyDown open={open} onClose={onClose}>
      <DialogTitle>{`Выберите родительскую категорию для ${item?.type === 'product' ? 'товара' : 'категории'}: ${item?.name}`}</DialogTitle>
      <DialogContent>
        <Box component="form" sx={{ display: 'flex', flexWrap: 'wrap', mt: 2 }}>
          <FormControl fullWidth>
            <InputLabel htmlFor="demo-dialog-native">Категория</InputLabel>
            <Select
              value={selected}
              native
              onChange={handleChange}
              input={<OutlinedInput label="Категория" id="demo-dialog-native" />}
            >
              <option aria-label="None" value="none" key="none">---Без категории---</option>
              {item?.type === 'category' && (
                <option value="root" key="root">---Корневой---</option>
              )}
              {categories?.map((category) => (
                <option value={category.id} key={category.id}>{category.name}</option>
              ))}
            </Select>
          </FormControl>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} >Cancel</Button>
        <Button disabled={!selected} onClick={onConfirm}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
};

DialogMoveProduct.defaulProps = {
  item: {},
};

export default DialogMoveProduct;
