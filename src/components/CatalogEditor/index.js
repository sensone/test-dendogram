import React from 'react';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

import Dendogram from 'containers/Dendogram';
import CategoriesList from 'containers/CategoriesList';
import ProductsList from 'containers/ProductsList';
import History from 'containers/History';
import Comments from 'containers/Comments';

import DialogMoveItem from 'containers/DialogMoveItem';
import DialogAddProduct from 'containers/DialogAddProduct';
import DialogAddCategory from 'containers/DialogAddCategory';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
  height: '100%',
}));

const CatelogEditor = () => (
  <>
    <Grid container spacing={2} sx={{ mb: 2 }}>
      <Grid item xs={12}>
        <History />
      </Grid>
      <Grid item xs={12}>
        <Item>
          <Dendogram />
        </Item>
      </Grid>
      <Grid item xs={6}>
        <Item>
          <CategoriesList />
        </Item>
      </Grid>
      <Grid item xs={6}>
        <Item>
          <ProductsList />
        </Item>
      </Grid>
      <Grid item xs={12}>
        <Item>
          <Comments />
        </Item>
      </Grid>
    </Grid>
    <DialogAddProduct />
    <DialogAddCategory />
    <DialogMoveItem />
  </>
);

export default CatelogEditor;
