import React, { useState, useCallback, useMemo } from 'react';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const DialogMoveProduct = ({ open, onAdd, onClose, categories, type }) => {
  const [selectedCategory, setSelectedCategory] = useState(undefined);
  const [name, setName] = useState(undefined);
  const [price, setPrice] = useState(undefined);

  const handleChangeCategory = useCallback((event) => (
    setSelectedCategory(event?.target?.value)
  ), [setSelectedCategory]);

  const handleChangeName = useCallback((event) => (
    setName(event?.target?.value)
  ), [setName]);

  const handleChangePrice = useCallback((event) => (
    setPrice(event?.target?.value)
  ), [setPrice]);

  const clearValues = useCallback(() => {
    setSelectedCategory(undefined);
    setName(undefined);
    setPrice(undefined);
  }, [setSelectedCategory, setName, setPrice]);

  const onConfirm = useCallback(() => {
    onAdd({ selectedCategory, name, price });
    clearValues();
  }, [selectedCategory, name, price, clearValues, onAdd]);

  const handleClose = useCallback(() => {
    clearValues();
    onClose();
  }, [onClose, clearValues]);

  const isDisabled = useMemo(() => {
    if (type === 'product') {
      return !name && !price;
    }

    return !name;
  }, [name, price, type]);

  return (
    <Dialog disableEscapeKeyDown open={open} onClose={handleClose}>
      <DialogTitle>{type === 'product' ? 'Добавление товара' : 'Добавление катерории'}</DialogTitle>
      <DialogContent>
        <Box component="form" sx={{ display: 'flex', flexWrap: 'wrap' }}>
          <FormControl fullWidth margin="normal">
            <TextField required id="outlined-basic" label="Название" variant="outlined" onChange={handleChangeName} />
          </FormControl>
          {type === 'product' && (
          <FormControl fullWidth margin="normal">
            <TextField id="outlined-basic" label="Цена" variant="outlined" onChange={handleChangePrice} />
          </FormControl>
          )}
          <FormControl fullWidth margin="normal">
            <InputLabel htmlFor="demo-dialog-native">Категория</InputLabel>
            <Select
              native
              onChange={handleChangeCategory}
              input={<OutlinedInput label="Категория" id="demo-dialog-native" />}
            >
              <option aria-label="None" value={undefined} key="none">---Без категории---</option>
              {type === 'category' && (
                <option value="root" key="root">---Корневой---</option>
              )}
              {categories?.map((category) => (
                <option value={category.id} key={category.id}>{category.name}</option>
              ))}
            </Select>
          </FormControl>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button disabled={isDisabled} onClick={onConfirm}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogMoveProduct;
