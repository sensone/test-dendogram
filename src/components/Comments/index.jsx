import React, { useState, useMemo, useCallback } from 'react';
import {
  Grid,
  Typography,
  Avatar,
  Pagination,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
} from '@mui/material';
import { commentsPerPage } from 'constants/comments';
import './styles.css';

const CommentsComponent = ({ commentsData }) => {
  const [currentPage, setCurrentPage] = useState(1);

  const totalPages = useMemo(() => Math.ceil(commentsData.length / commentsPerPage), [commentsData]);

  const indexOfLastComment = useMemo(() => currentPage * commentsPerPage, [currentPage]);
  const indexOfFirstComment = useMemo(() => indexOfLastComment - commentsPerPage, [indexOfLastComment]);
  const currentComments = useMemo(() => commentsData.slice(
    indexOfFirstComment,
    indexOfLastComment
  ), [commentsData, indexOfFirstComment, indexOfLastComment]);

  const handlePageChange = useCallback((event, newPage) => {
    setCurrentPage(newPage);
  }, [setCurrentPage]);

  return (
    <>
      <Typography variant="h6" gutterBottom>
        Комментарии
      </Typography>
      <Grid container spacing={2} className="comments-container">
        <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
          {currentComments.map((comment, index) => (
            <div key={comment.id}>
              <ListItem alignItems="flex-start" key={comment.id}>
                <ListItemAvatar>
                  <Avatar
                    alt={comment.user.name}
                    src={comment.user.avatar}
                    className="comments-avatar"
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={comment.user.name}
                  secondary={
                    <Typography variant="body1">{comment.text}</Typography>
                  }
                />
              </ListItem>
              {index !== currentComments.length - 1 && (
                <Divider variant="inset" component="li" />
              )}
            </div>
          ))}
        </List>

        <Grid
          container
          justifyContent="center"
          className="pagination-container"
        >
          <Pagination
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default CommentsComponent;
