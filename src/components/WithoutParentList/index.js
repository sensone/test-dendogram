import React from 'react';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import FolderIcon from '@mui/icons-material/Folder';
import KeyboardDoubleArrowRightSharpIcon from '@mui/icons-material/KeyboardDoubleArrowRightSharp';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const WithoutParentList = ({ list, onOpenMoveDialog, onOpenAddDialog, type, onRemove }) => (
  <div>
    <Typography variant="h6" gutterBottom>
      {`${type === 'product' ? 'Товары' : 'Категории'} без категорий(${list.length})`}
    </Typography>
    <Box>
      <Button onClick={onOpenAddDialog}>{`Добавить ${type === 'product' ? 'товар' : 'категорию'}`}</Button>
    </Box>
    <List>
      {list.map((item) => (
        <ListItem
          key={item.id}
          secondaryAction={
            <>
              <IconButton edge="end" aria-label="удалить" onClick={() => onOpenMoveDialog(item.id)}>
                <KeyboardDoubleArrowRightSharpIcon />
              </IconButton>
              <IconButton edge="end" aria-label="удалить" onClick={() => (onRemove(item.id))}>
                <DeleteIcon />
              </IconButton>
            </>
          }
          >
          <ListItemAvatar>
            <Avatar>
              <FolderIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={item.name}
          />
        </ListItem>
      ))}
    </List>
  </div>
);

export default WithoutParentList;
