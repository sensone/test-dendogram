import React, { useMemo } from 'react';
import Button from '@mui/material/Button';

const History = ({ index, count, goBack, goNext }) => {
  const isDisabledBack = useMemo(() => (index === 0), [index]);
  const isDisabledNext = useMemo(() => ((count - 1) === index), [index, count]);

  return (
    <div>
      <Button disabled={isDisabledBack} onClick={goBack}>Назад</Button>
      <Button disabled={isDisabledNext} onClick={goNext}>Вперед</Button>
    </div>
  );
};


export default History;
