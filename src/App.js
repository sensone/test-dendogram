
import React from 'react';
import { Provider } from 'react-redux';
import Container from '@mui/material/Container';

import CatalogEditor from 'components/CatalogEditor';
import configureStore from 'store';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <Container maxWidth="xl">
        <CatalogEditor />
      </Container>
    </Provider>
  );
};

export default App;
