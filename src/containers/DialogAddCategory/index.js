import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import DialogAdd from 'components/DialogAdd';

import { getDialogAddCategory, getCategoriesWithParent } from 'store/selectors/catalog';
import { addCategory, closeAddCategoryDialog } from 'store/actions/catalog';

const DialogAddContainer = () => {
  const dispatch = useDispatch();
  const { open } = useSelector(getDialogAddCategory);
  const categories = useSelector(getCategoriesWithParent);

  const onAdd = useCallback((payload) => (
    dispatch(addCategory(payload))
  ), [dispatch]);

  const onClose = useCallback(() => (
    dispatch(closeAddCategoryDialog())
  ), [dispatch]);

  return (
    <DialogAdd
      open={open}
      onAdd={onAdd}
      onClose={onClose}
      categories={categories}
      type="category"
    />
  );
};

export default DialogAddContainer;
