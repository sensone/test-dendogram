import React from 'react';
import { useSelector } from 'react-redux';

import Comments from 'components/Comments';
import { getComments } from 'store/selectors/comments';

const CommentsContainer = () => {
  const comments = useSelector(getComments);

  return <Comments commentsData={comments} />;
};

export default CommentsContainer;
