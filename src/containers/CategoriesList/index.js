import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import WithoutParentList from 'components/WithoutParentList';
import { getCategoriesWithoutParent } from 'store/selectors/catalog';
import {
  openMoveItemDialog,
  openAddCategoryDialog,
  removeItem,
} from 'store/actions/catalog';

const WithoutParentListContainer = () => {
  const dispatch = useDispatch();
  const categoriesWithoutParent = useSelector(getCategoriesWithoutParent);

  const onOpenMoveCategoryDialog = useCallback((id) => (
    dispatch(openMoveItemDialog({ id }))
  ), [dispatch]);

  const onOpenAddCategoryDialog = useCallback(() => (
    dispatch(openAddCategoryDialog())
  ), [dispatch]);

  const onRemove = useCallback((id) => (
    dispatch(removeItem({ id }))
  ), [dispatch]);

  return (
    <WithoutParentList
      list={categoriesWithoutParent}
      onOpenMoveDialog={onOpenMoveCategoryDialog}
      onOpenAddDialog={onOpenAddCategoryDialog}
      onRemove={onRemove}
      type="category"
    />
  );
};

export default WithoutParentListContainer;
