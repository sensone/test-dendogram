import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { filter } from 'ramda';
import DialogMove from 'components/DialogMove';

import { getDialogMoveItem, getCategoriesWithParent, getItemById } from 'store/selectors/catalog';
import { moveItem, closeMoveItemDialog } from 'store/actions/catalog';
import { getAllIds } from 'helpers/catalog';

const DialogMoveContainer = () => {
  const dispatch = useDispatch();
  const { open, id, treeObj } = useSelector(getDialogMoveItem);
  const allCategories = useSelector(getCategoriesWithParent);
  const item = useSelector(getItemById(id));

  const onMove = useCallback((parentId) => (
    dispatch(moveItem({ id, parentId }))
  ), [dispatch, id]);

  const onClose = useCallback(() => (
    dispatch(closeMoveItemDialog())
  ), [dispatch]);

  const categories = useMemo(() => {
    const childrenIds = treeObj ? getAllIds(treeObj) : [];

    return filter((item) => (childrenIds.indexOf(item.id)) < 0, allCategories);
  }, [treeObj, allCategories]);

  return (
    <DialogMove
      open={open}
      onMove={onMove}
      onClose={onClose}
      categories={categories}
      item={item}
    />
  );
};

export default DialogMoveContainer;
