import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import History from 'components/History';
import { getHistoryIndex, getItemsHistoryCount } from 'store/selectors/catalog';
import { goBack, goNext } from 'store/actions/catalog';

const HistoryContainer = () => {
  const dispatch = useDispatch();
  const historyIndex = useSelector(getHistoryIndex);
  const count = useSelector(getItemsHistoryCount);

  const onGoBack = useCallback(() => (
    dispatch(goBack())
  ), [dispatch]);

  const onGoNext = useCallback(() => (
    dispatch(goNext())
  ), [dispatch]);

  return (
    <History
      index={historyIndex}
      count={count}
      goBack={onGoBack}
      goNext={onGoNext}
    />
  )
};

export default HistoryContainer;
