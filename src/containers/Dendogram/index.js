import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Dendogram from 'components/Dendogram';
import { getCategoryTree } from 'store/selectors/catalog';

import {
  openMoveItemDialog,
} from 'store/actions/catalog';

const DendogramContainer = () => {
  const catalogTree = useSelector(getCategoryTree);
  const dispatch = useDispatch();

  const onOpenMoveDialog = useCallback((id, treeObj) => (
    dispatch(openMoveItemDialog({ id, treeObj }))
  ), [dispatch]);

  const data = {
    name: 'root',
    children: catalogTree,
  };

  return (
    <Dendogram
      data={data}
      onOpenMoveDialog={onOpenMoveDialog}
      width={1200}
    />
  )
};

export default DendogramContainer;
