import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import DialogAdd from 'components/DialogAdd';

import { getDialogAddProduct, getCategoriesWithParent } from 'store/selectors/catalog';
import { addProduct, closeAddProductDialog } from 'store/actions/catalog';

const DialogAddContainer = () => {
  const dispatch = useDispatch();
  const { open } = useSelector(getDialogAddProduct);
  const categories = useSelector(getCategoriesWithParent);

  const onAdd = useCallback((payload) => (
    dispatch(addProduct(payload))
  ), [dispatch]);

  const onClose = useCallback(() => (
    dispatch(closeAddProductDialog())
  ), [dispatch]);

  return (
    <DialogAdd
      open={open}
      onAdd={onAdd}
      onClose={onClose}
      categories={categories}
      type="product"
    />
  );
};

export default DialogAddContainer;
