import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import WithoutParentList from 'components/WithoutParentList';
import { getProductsWithoutParent } from 'store/selectors/catalog';
import {
  openMoveItemDialog,
  openAddProductDialog,
  removeItem,
} from 'store/actions/catalog';

const WithoutParentListContainer = () => {
  const dispatch = useDispatch();
  const productsWithoutParent = useSelector(getProductsWithoutParent);

  const onOpenMoveProductDialog = useCallback((id) => (
    dispatch(openMoveItemDialog({ id }))
  ), [dispatch]);

  const onRemove = useCallback((id) => (
    dispatch(removeItem({ id }))
  ), [dispatch]);

  const onOpenAddProductDialog = useCallback(() => (
    dispatch(openAddProductDialog())
  ), [dispatch]);

  return (
    <WithoutParentList
      list={productsWithoutParent}
      onOpenMoveDialog={onOpenMoveProductDialog}
      onOpenAddDialog={onOpenAddProductDialog}
      onRemove={onRemove}
      type="product"
    />
  );
};

export default WithoutParentListContainer;
