import { createAction } from '@reduxjs/toolkit';

export const addProduct = createAction('CATALOG:ADD_PRODUCT');
export const addCategory = createAction('CATALOG:ADD_CATALOG');
export const removeItem = createAction('CATALOG:REMOVE_ITEM')

// DIALOGS

export const moveItem = createAction('CATALOG:MOVE_ITEM');

export const openMoveItemDialog = createAction('CATALOG:OPEN_MOVE_ITEM_DIALOG');
export const closeMoveItemDialog = createAction('CATALOG:CLOSE_MOVE_ITEM_DIALOG');

export const openAddProductDialog = createAction('CATALOG:OPEN_ADD_PRODUCT_DIALOG');
export const closeAddProductDialog = createAction('CATALOG:CLOSE_ADD_PRODUCT_DIALOG');

export const openAddCategoryDialog = createAction('CATALOG:OPEN_ADD_CATALOG_DIALOG');
export const closeAddCategoryDialog = createAction('CATALOG:CLOSE_ADD_CATALOG_DIALOG');

// HISTORY
export const goBack = createAction('CATALOG:HISTORY_GO_BACK');
export const goNext = createAction('CATALOG:HISTORY_GO_NEXT');
