import { configureStore } from '@reduxjs/toolkit';

import rootReducer from './reducers';

const getStore = () => {
  const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV === 'development',
  });

  return store;
};

export default getStore;
