import { createSelector } from '@reduxjs/toolkit';
import { prop, filter, propEq, find, length } from 'ramda';

import { transformCategoryToTree } from 'helpers/catalog';

const localState = prop('catalog');

export const getItemsHistory = createSelector(
  localState,
  prop('items'),
);

export const getItemsHistoryCount = createSelector(
  getItemsHistory,
  length,
);

export const getHistoryIndex = createSelector(
  localState,
  prop('historyIndex'),
);

export const getCurrentCatalog = createSelector(
  getItemsHistory,
  getHistoryIndex,
  (items, index) => (items[index]),
);

export const getTransformedCategoryToTree = createSelector(
  getCurrentCatalog,
  transformCategoryToTree,
);

export const getCategoryTree = createSelector(
  getTransformedCategoryToTree,
  prop('catalogTree'),
);

export const getItemsWithoutParent = createSelector(
  getTransformedCategoryToTree,
  prop('withoutParent'),
);

export const getCategories = createSelector(
  getCurrentCatalog,
  filter(propEq('category', 'type')),
);

export const getItemById = (id) => createSelector(
  getCurrentCatalog,
  find(propEq(id, 'id')),
);

export const getCategoriesWithParent = createSelector(
  getCategories,
  filter(prop('parentId')),
);

export const getProductsWithoutParent = createSelector(
  getItemsWithoutParent,
  filter(propEq('product', 'type')),
);

export const getCategoriesWithoutParent = createSelector(
  getItemsWithoutParent,
  filter(propEq('category', 'type')),
);

// DIALOGS
export const getDialogs = createSelector(
  localState,
  prop('dialogs'),
);

export const getDialogMoveItem = createSelector(
  getDialogs,
  prop('moveItem'),
);

export const getDialogAddProduct = createSelector(
  getDialogs,
  prop('addProduct'),
);

export const getDialogAddCategory = createSelector(
  getDialogs,
  prop('addCategory'),
);
