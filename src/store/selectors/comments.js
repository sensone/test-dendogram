import { createSelector } from '@reduxjs/toolkit';
import { prop } from 'ramda';

export const getComments = createSelector(
  prop('comments'),
  data => data,
);
