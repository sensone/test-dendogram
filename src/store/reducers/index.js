import catalog from './catalog';
import comments from './comments';

const root = {
  catalog,
  comments,
};

export default root;
