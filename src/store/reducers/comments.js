import { createSlice } from '@reduxjs/toolkit';
import comments from 'mocks/comments';

const initialState = comments;

const commentsSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {},
});

export default commentsSlice.reducer;