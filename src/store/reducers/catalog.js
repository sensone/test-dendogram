import { assoc, assocPath, pipe, propEq, map, reject, findIndex } from 'ramda';
import { createReducer } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

import categories from 'mocks/categories';
import products from 'mocks/products';
import { getHistoryState, getAllIds } from 'helpers/catalog';

import {
  addProduct,
  addCategory,
  removeItem,
  moveItem,

  openMoveItemDialog,
  closeMoveItemDialog,

  openAddProductDialog,
  closeAddProductDialog,

  openAddCategoryDialog,
  closeAddCategoryDialog,

  goBack,
  goNext,
} from 'store/actions/catalog';

const initialState = {
  items: [[...categories, ...products]],
  map: [],
  historyIndex: 0,
  dialogs: {
    moveItem: { open: false, id: undefined, treeObj: null },
    addProduct: { open: false },
    addCategory: { open: false },
  },
};

export default createReducer(initialState, (builder) => {
  builder.addCase(addProduct, (state, { payload: { selectedCategory, name, price } }) => {
    const { historyIndex, itemsHistory } = getHistoryState(state);
    const newItems = [...state.items[historyIndex], {
      id: `product_${uuidv4()}`,
      parentId: selectedCategory,
      type: 'product',
      name,
      price,
    }];

    return pipe(
      assoc('items', [...itemsHistory, newItems]),
      assoc('historyIndex', historyIndex + 1),
      assocPath(['dialogs', 'addProduct', 'open'], false),
    )(state);
  });

  builder.addCase(addCategory, (state, { payload: { selectedCategory, name } }) => {
    const { historyIndex, itemsHistory } = getHistoryState(state);
    const newItems = [...state.items[historyIndex], {
      id: `category_${uuidv4()}`,
      parentId: selectedCategory,
      type: 'category',
      name,
      children: [],
    }];

    return pipe(
      assoc('items', [...itemsHistory, newItems]),
      assoc('historyIndex', historyIndex + 1),
      assocPath(['dialogs', 'addCategory', 'open'], false),
    )(state);
  });

  builder.addCase(removeItem, (state, { payload: { id } }) => {
    const { historyIndex, itemsHistory } = getHistoryState(state);
    const newItems = reject(propEq(id, 'id'))([...state.items[historyIndex]]);

    return pipe(
      assoc('items', [...itemsHistory, newItems]),
      assoc('historyIndex', historyIndex + 1),
    )(state);
  });

  builder.addCase(moveItem, (state, { payload: { id, parentId } }) => {
    const { historyIndex, itemsHistory } = getHistoryState(state);
    const itemIndex = findIndex(propEq(id, 'id'))(state.items[historyIndex]);
    const childrenIds = parentId ? [] : getAllIds(state.dialogs.moveItem.treeObj);
    const newItems = pipe(
      assocPath([itemIndex, 'parentId'], parentId),
      map((item) => (
        childrenIds.indexOf(item.id) >= 0
        ? assoc('parentId', undefined, item)
        : item
      ))
    )(state.items[historyIndex]);

    return pipe(
      assoc('items', [...itemsHistory, newItems]),
      assoc('historyIndex', historyIndex + 1),
      assocPath(['dialogs', 'moveItem', 'open'], false),
      assocPath(['dialogs', 'moveItem', 'id'], undefined),
      assocPath(['dialogs', 'moveItem', 'treeObj'], null),
    )(state);
  });

  builder.addCase(openMoveItemDialog, (state, { payload: { id, treeObj = null } }) => pipe(
    assocPath(['dialogs', 'moveItem', 'open'], true),
    assocPath(['dialogs', 'moveItem', 'id'], id),
    assocPath(['dialogs', 'moveItem', 'treeObj'], treeObj),
  )(state));

  builder.addCase(closeMoveItemDialog, (state) => pipe(
    assocPath(['dialogs', 'moveItem', 'open'], false),
    assocPath(['dialogs', 'moveItem', 'id'], undefined),
    assocPath(['dialogs', 'moveItem', 'treeObj'], null),
  )(state));

  builder.addCase(openAddProductDialog, assocPath(['dialogs', 'addProduct', 'open'], true));
  builder.addCase(closeAddProductDialog, assocPath(['dialogs', 'addProduct', 'open'], false));
  builder.addCase(openAddCategoryDialog, assocPath(['dialogs', 'addCategory', 'open'], true));
  builder.addCase(closeAddCategoryDialog, assocPath(['dialogs', 'addCategory', 'open'], false));

  builder.addCase(goBack, (state) => (assoc('historyIndex', state.historyIndex - 1, state)));
  builder.addCase(goNext, (state) => (assoc('historyIndex', state.historyIndex + 1, state)));
});
