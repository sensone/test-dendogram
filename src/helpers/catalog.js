import { slice } from 'ramda';

export const transformCategoryToTree = (catalog = []) => {
  const map = catalog.reduce((acc, item) => {
    acc[item.id] = {
      id: item.id,
      name: item.name,
      type: item.type,
      children: item.type === 'product' ? null : [],
    };

    return acc;
  }, {});

  const result = catalog.reduce((acc, item) => {
    if (item.parentId && map[item.parentId]) {
      map[item.parentId].children.push(map[item.id]);
    } else if (item.type === "category" && item.parentId) {
      acc.catalogTree.push(map[item.id]);
    } else {
      acc.withoutParent.push(item);
    }

    return acc;
  }, { catalogTree: [], withoutParent: []});

  return result;
};

export const getHistoryState = (state) => {
  const { historyIndex } = state;
  const nextIndex = historyIndex + 1;
  const itemsHistory = slice(0, nextIndex)(state.items);

  return { historyIndex, itemsHistory };
};

export const getAllIds = (obj) => {
  let ids = [obj.id];

  if (obj.children) {
    obj.children.forEach(child => {
      ids = ids.concat(getAllIds(child));
    });
  }
  return ids;
};
